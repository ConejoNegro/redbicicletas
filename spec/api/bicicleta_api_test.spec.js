var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Bicicleta API', () => {
    describe('get Bicis /', ()=>{
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1,'verde', 'playera', [-34.91353810164513, -57.9544583250727]);
            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST Bicis /create', () => {
        it('Status 200', (done) => {
            var header = {'content-type': 'applicaation/json'};
            var unaBici = { "id": 9, "color": "azul", "modelo": "carrera", "lat": -34.902372, "lng": -57.954513};
            request.post({
                headers: header,
                body: unaBici,
                url: 'http://localhost:3000/api/bicicletas/create'
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(9).color).toBe('azul');
                done();
            });
        });
    });
});