var mongoose = require ('mongoose');
var Bicicleta = require('../../models/bicicleta');



describe('Testeo bicicleta', function(){
    beforeEach(function(done){
        mongoose.connect('mongodb://localhost/testdb', {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('estamos conectados a la db test')
        });
        done();
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });    

    describe('Bicicleta.createInstance', () => {
        it('Crear una instancia de bici', () =>{
            var unaBici = Bicicleta.createInstance(1,"verde", "playera", [-34.9135, -57.9544]);

            expect(unaBici.code).toBe(1);
            expect(unaBici.color).toBe("verde");
            expect(unaBici.modelo).toBe("playera");
            expect(unaBici.ubicacion[0]).toEqual(-34.9135);
            expect(unaBici.ubicacion[1]).toEqual(-57.9544);
        })
    });


    describe('Bicicleta.createInstance', () => {
        it('Comienza vacia', (done) =>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });




});
/* beforeEach(() => (Bicicleta.allBicis = []));

describe('Bicicleta.AllBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    });
});

describe('Bicicleta.add', () => {
    it('agregamos 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'verde', 'playera', [-34.91353810164513, -57.9544583250727]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1)
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var b = new Bicicleta(2,'verde', 'playera', [-34.91353810164513, -57.9544583250727]);
        var a = new Bicicleta(1,'verde', 'playera', [-34.91353810164513, -57.9544583250727]);
        Bicicleta.add(b);
        Bicicleta.add(a);

        var unaBici = Bicicleta.findById(1);
        expect(unaBici.id).toBe(1);
        expect(unaBici.color).toBe(a.color);
        expect(unaBici.modelo).toBe(a.modelo);
        
    });
});

describe('Bicicleta.removeById', () => {
    it('debe devolver la bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var b = new Bicicleta(2,'verde', 'playera', [-34.91353810164513, -57.9544583250727]);
        var a = new Bicicleta(1,'verde', 'playera', [-34.91353810164513, -57.9544583250727]);
        Bicicleta.add(b);
        Bicicleta.add(a);

        var unaBici = Bicicleta.removeById(1);
        expect(unaBici).toBe(null);
        expect(unaBici.color).toBe(a.color);
        expect(unaBici.modelo).toBe(a.modelo);
        
    });
});
 */